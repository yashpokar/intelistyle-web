import React, { useState } from 'react';
import { useListing } from './../context/ListingContext';

export default function HomePage() {
  const [query, setQuery] = useState('');
  const { onQuery, products, isSubmmited, isLoading } = useListing();

  const onSubmit = e => {
    e.preventDefault();

    onQuery(query);
  };

	return (
    <div className="flex flex-col justify-center items-center w-full min-h-screen font-display antialiased font-normal">
      <form onSubmit={onSubmit} className="flex flex-col gap-y-8">
        <div className="text-center">
          <h3 className="text-5xl font-bold my-8">
            Google <span className="text-indigo-500">"Garments"</span>
          </h3>
        </div>

        <div className="flex gap-x-4">
          <div className="">
            <input
              type="text"
              className="py-2.5 px-2 w-80 rounded-md focus:outline-none border border-indigo-300 focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              aria-label="Search"
              onChange={e => setQuery(e.target.value.trim())}
            />
          </div>

          <div className="">
            <button className="flex items-center py-3 px-6 rounded-md bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 hover:bg-indigo-800">
              <svg
                className="w-5 h-5 mr-1 text-white"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                />
              </svg>

              <span className="font-semibold text-white">
                Search
              </span>
            </button>
          </div>
        </div>
      </form>

      {products.length ? (
        <div className="w-10/12 mx-auto mt-16 mb-6">
          <ul className="grid grid-cols-2 gap-8">
            {products.map((product, idx) => {
              // TODO :: https://via.placeholder.com/450/FFFFFF%20C/000000/O%20 should come from backend
              let image = product.image_urls && product.image_urls.length ? product.image_urls[0] : 'https://via.placeholder.com/450/FFFFFF%20C/000000/O%20';

              return (
                <li className="flex gap-4 hover:shadow-xl cursor-pointer transition duration-200 border border-gray-200 p-3 rounded" key={idx}>
                  <div className="flex w-40 rounded-l rounded-b overflow-hidden">
                    <img
                      src={image}
                      alt={product.title}
                      className=""
                    />
                  </div>

                  <div className="flex flex-col">
                    <a href={product.url} className="">
                      <h4 className="font-medium text-xl text-gray-600 hover:text-blue-500">
                        {product.title}
                      </h4>
                    </a>

                    <p className="text-gray-500 text-ellipsis overflow-hidden">{product.description}</p>

                    <div className="">
                      <span className="font-bold text-indigo-500 text-lg">
                        {product.price} {product.currency_code}
                      </span>
                    </div>

                    <div className="mt-2">
                      <span className="bg-indigo-700 text-white text-sm font-medium tracking-wide rounded px-2 py-1">
                        {product.brand}
                      </span>
                    </div>

                    <div className="mt-2">
                      <span className="">
                        {product.gender}
                      </span>
                    </div>
                  </div>
                </li>
              )}
            )}
          </ul>
        </div>
      ) : ! isLoading && isSubmmited ? (
        <div className="mt-16">
          No results found.
        </div>
       ) : isLoading ? (
         <div className="mt-16">
           Loading...
         </div>
       ) : null}
    </div>
  );
}
