import React, { lazy, Suspense } from 'react';
import Splash from './pages/Splash';
import { ListingProvider } from './context/ListingContext';

const HomePage = lazy(() => import('./pages/Home'));

function App() {
  return (
    <div>
      <Suspense fallback={<Splash />}>
        <ListingProvider>
          <HomePage />
        </ListingProvider>
      </Suspense>
    </div>
  );
}

export default App;
