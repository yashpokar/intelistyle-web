import React, { createContext, useContext, useReducer, useMemo } from 'react';
import axios from 'axios';
import { LIST_PRODUCTS, SEARCH } from './../constants';

const ListingContext = createContext(null);

export function ListingProvider({ children }) {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case SEARCH:
        return {
          ...state,
          isLoading: true,
          isSubmmited: true,
        };

      case LIST_PRODUCTS:
        return {
          ...state,
          isLoading: false,
          products: action.products,
        };

      default:
        return state;
    }
  }, {
    query: '',
    isLoading: false,
    isSubmmited: false,
    products: [],
  });

  const actions = useMemo(() => ({
    onQuery: async query => {
      try {
        dispatch({ type: SEARCH });

        const { data } = await axios.get('/search', { params: { query } });

        dispatch({
          type: LIST_PRODUCTS,
          products: data.products,
        });
      } catch (err) {
        // handle error
      }
    },
  }), []);

  return (
    <ListingContext.Provider value={{...state, ...actions}}>
      {children}
    </ListingContext.Provider>
  );
}

export function useListing() {
  const context = useContext(ListingContext);

  if (context === undefined) {
    throw new Error('useListing is only available inside ListingProvider.');
  }

  return context;
}
